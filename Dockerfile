FROM docker.io/library/debian:bookworm
LABEL name="docker-linter"
LABEL url="https://gitlab.com/thomasdstewart-infra/docker-linter/"
LABEL maintainer="thomas@stewarts.org.uk"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update \
    && apt-get -y --no-install-recommends install ca-certificates curl git jq shellcheck yamllint \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && curl -sL "$(curl -s https://api.github.com/repos/tfverch/tfvc/releases/latest | awk '/browser_download_url.*Linux_x86_64/{print $2}' | xargs )" | tar xvzf - -C /usr/local/bin tfvc

CMD [""]
