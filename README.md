# docker-linter

GitLab: [![pipeline status](https://gitlab.com/thomasdstewart-infra/docker-linter/badges/main/pipeline.svg)](https://gitlab.com/thomasdstewart-infra/docker-linter/-/commits/main)

Gitea: [![Build Status](https://drone.stewarts.org.uk/api/badges/thomasdstewart-infra/docker-linter/status.svg)](https://drone.stewarts.org.uk/thomasdstewart-infra/docker-linter)

Docker image with various linters installed:
 * shellcheck
 * yamllint
 * ansible-lint
